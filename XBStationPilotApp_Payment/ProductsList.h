#ifndef PRODUCTSLIST_H
#define PRODUCTSLIST_H

#include <QObject>
#include <QDebug>
#include <QVector>

struct DataProducts
{
//    double id;
    QString name;
    QString title;
    QString description;
    double price;
//    double value;
//    QString createdAt;
//    QString updatedAt;
//    QString deletedAt;
//    double permissionId;
//    double featureId;
};

class ProductsList : public QObject
{
    Q_OBJECT
public:
    explicit ProductsList(QObject *parent = nullptr);
    QVector< DataProducts > dataProducts() const;

    bool setProductAt(int index, const DataProducts &product);

signals:
//    void preProductAppended();
//    void postProductAppended();

//    void preProductRemoved(int dataSize);
//    void postProductRemoved();
public slots:
    void appendProducts(QVector< DataProducts > m_dataProductsVector);

private:
    QVector< DataProducts > m_dataProducts;
};

#endif // PRODUCTSLIST_H
