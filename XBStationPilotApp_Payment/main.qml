import QtQuick
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1



Window {
    width: 1530
    height: 800
    visible: true
    title: qsTr("XBStationPilotApp Payment")
    color: "#F9FAFA"

    RowLayout {
        id: root
        anchors.fill: parent
        ColumnLayout {
            id: area_infor_user
            anchors.top: root.top
            anchors.bottom: root.bottom

            Rectangle {
                id: container_infor
                width: root.width / 3
                height: root.height
                anchors.top: area_infor_user.top
                anchors.topMargin: 20
                anchors.bottom: area_infor_user.bottom
                anchors.bottomMargin: 20
                anchors.left: area_infor_user.left
                anchors.leftMargin: 20
                color: "white"
                border.color: "#F0F0F0"
                border.width: 2



                Rectangle {
                    id: content_infor
                    anchors.top: container_infor.top
                    anchors.topMargin: 30
                    anchors.bottom: container_infor.bottom
                    anchors.bottomMargin: 30
                    anchors.left: container_infor.left
                    anchors.leftMargin: 50
                    Label {
                        id: lblUser
                        text: "User"
                        font.pixelSize: 38
                        color: "#1D707D"
                    }
                    Rectangle {
                        id: infor_user
                        anchors.top: lblUser.bottom
                        anchors.topMargin: 15
                        anchors.left: content_infor.left
                        Text {
                            text: "Information Customer"
                            font.bold: true
                            font.pixelSize: 18
                        }

                        GridLayout {
                            columns: 1
                            anchors.top: infor_user.bottom
                            anchors.topMargin: 40
                            rowSpacing: 20
                            Rectangle {
                                border.color: "#E0E0E0"
                                border.width: 1
                                Layout.preferredWidth: container_infor.width - 100
                                Layout.preferredHeight: container_infor.height / 15
                                Rectangle {
                                    anchors.top: parent.top
                                    anchors.topMargin: 5
                                    anchors.left: parent.left
                                    anchors.leftMargin: 10

                                    Label {
                                        id: lblUserName
                                        text: qsTr("User Name")
                                        color: "#42464E" //#C7BDB9
                                    }
                                    Text {
                                        id: txtUserName
                                        anchors.top: lblUserName.bottom
                                        anchors.topMargin: 5
                                        text: processData.userName
                                        font.pixelSize: 14
                                        font.bold: true
                                    }
                                }

                            }
                            Rectangle {
                                border.color: "#E0E0E0"
                                border.width: 1
                                Layout.preferredWidth: container_infor.width - 100
                                Layout.preferredHeight: container_infor.height / 15
                                Rectangle {
                                    anchors.top: parent.top
                                    anchors.topMargin: 5
                                    anchors.left: parent.left
                                    anchors.leftMargin: 10

                                    Label {
                                        id: lblEmail
                                        text: qsTr("Email")
                                        color: "#42464E"
                                    }
                                    Text {
                                        id: txtEmail
                                        anchors.top: lblEmail.bottom
                                        anchors.topMargin: 5
                                        text: processData.email
                                        font.pixelSize: 14
                                        font.bold: true
                                    }
                                }
                            }
                            Rectangle {
                                border.color: "#E0E0E0"
                                border.width: 1
                                Layout.preferredWidth: container_infor.width - 100
                                Layout.preferredHeight: container_infor.height / 15
                                Rectangle {
                                    anchors.top: parent.top
                                    anchors.topMargin: 5
                                    anchors.left: parent.left
                                    anchors.leftMargin: 10

                                    Label {
                                        id: lblLimitation
                                        text: qsTr("Account Expiration")
                                        color: "#42464E"
                                    }
                                    Text {
                                        id: txtLimitation
                                        anchors.top: lblLimitation.bottom
                                        anchors.topMargin: 5
                                        text: (new Date(processData.limitation)).toUTCString() //javascript timestamp to utc
                                        font.pixelSize: 14
                                        font.bold: true
                                    }
                                }
                            }
                            Rectangle {
                                border.color: "#E0E0E0"
                                border.width: 1
                                Layout.preferredWidth: container_infor.width - 100
                                Layout.preferredHeight: container_infor.height / 15
                                Rectangle {
                                    anchors.top: parent.top
                                    anchors.topMargin: 5
                                    anchors.left: parent.left
                                    anchors.leftMargin: 10

                                    Label {
                                        id: lblConnectionLimitation
                                        text: qsTr("Maximum streaming sharing connection amount")
                                        color: "#42464E"
                                    }
                                    Text {
                                        id: txtConnectionLimitation
                                        anchors.top: lblConnectionLimitation.bottom
                                        anchors.topMargin: 5
                                        text: processData.connectionLimitation
                                        font.pixelSize: 14
                                        font.bold: true
                                    }
                                }
                            }
                            Rectangle {
                                border.color: "#E0E0E0"
                                border.width: 1
                                Layout.preferredWidth: container_infor.width - 100
                                Layout.preferredHeight: container_infor.height / 15
                                Rectangle {
                                    anchors.top: parent.top
                                    anchors.topMargin: 5
                                    anchors.left: parent.left
                                    anchors.leftMargin: 10

                                    Label {
                                        id: lblSecondaryConnectionLimitation
                                        text: qsTr("Maximum secondary pilot connection amount")
                                        color: "#42464E"
                                    }
                                    Text {
                                        id: txtSecondaryConnectionLimitation
                                        anchors.top: lblSecondaryConnectionLimitation.bottom
                                        anchors.topMargin: 5
                                        text: processData.secondaryConnectionLimitation
                                        font.pixelSize: 14
                                        font.bold: true
                                    }
                                }
                            }
                            Rectangle {
                                border.color: "#E0E0E0"
                                border.width: 1
                                Layout.preferredWidth: container_infor.width - 100
                                Layout.preferredHeight: container_infor.height / 7
                                Rectangle {
                                    anchors.top: parent.top
                                    anchors.topMargin: 5
                                    anchors.left: parent.left
                                    anchors.leftMargin: 10

                                    Label {
                                        id: lblPermission
                                        text: qsTr("Permission")
                                        color: "#42464E"
                                    }
                                    Rectangle {
                                        id: content_precision_landing
                                        anchors.top: lblPermission.bottom
                                        anchors.topMargin: 5
                                        Label {
                                            id: lblPrecisionLanding
                                            text: qsTr("Precision Landing feature: ")
                                        }
                                        Text {
                                            id: txtPrecisionLanding
                                            anchors.left: lblPrecisionLanding.right
                                            anchors.leftMargin: 5
                                            anchors.bottom: lblPrecisionLanding.bottom
                                            anchors.bottomMargin: -1
                                            text: processData.permissionPrecisionLanding === "1" ? "Yes" : "No"
                                            font.pixelSize: 14
                                            font.bold: true
                                        }
                                    }
                                    Rectangle {
                                        id: content_delivery
                                        anchors.top: content_precision_landing.bottom
                                        anchors.topMargin: 20
                                        Label {
                                            id: lblDelivery
                                            text: qsTr("Delivery feature: ")
                                        }
                                        Text {
                                            id: txtDelivery
                                            anchors.left: lblDelivery.right
                                            anchors.leftMargin: 5
                                            anchors.bottom: lblDelivery.bottom
                                            anchors.bottomMargin: -1
                                            text: processData.permissionDelivery === "1" ? "Yes" : "No"
                                            font.pixelSize: 14
                                            font.bold: true
                                        }
                                    }

                                    Rectangle {
                                        id: content_geo_tagging
                                        anchors.top: content_delivery.bottom
                                        anchors.topMargin: 20
//                                        anchors.left: content_precision_landing.right
//                                        anchors.leftMargin: 40
                                        Label {
                                            id: lblGeoTagging
                                            text: qsTr("Geo Tagging feature: ")
                                        }
                                        Text {
                                            id: txtGeoTagging
                                            anchors.left: lblGeoTagging.right
                                            anchors.leftMargin: 5
                                            anchors.bottom: lblGeoTagging.bottom
                                            anchors.bottomMargin: -1
                                            text: processData.permissionGeoTagging === "1" ? "Yes" : "No"
                                            font.pixelSize: 14
                                            font.bold: true
                                        }
                                    }
                                    Rectangle {
                                        id: content_ntrip
                                        anchors.top: content_geo_tagging.bottom
                                        anchors.topMargin: 20
                                        Label {
                                            id: lblNtrip
                                            text: qsTr("Ntrip feature: ")
                                        }
                                        Text {
                                            id: txtNtrip
                                            anchors.left: lblNtrip.right
                                            anchors.leftMargin: 5
                                            anchors.bottom: lblNtrip.bottom
                                            anchors.bottomMargin: -1
                                            text: processData.permissionNtrip === "1" ? "Yes" : "No"
                                            font.pixelSize: 14
                                            font.bold: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        ColumnLayout {
            id: area_list_products
            anchors.fill: parent

            Rectangle {
                id: container_list
                width: root.width * 2 / 3 //root.width * 2 / 3 - 25
                height: root.height
                color: "#F9FAFA"
//                color: "orange"
                anchors.top: area_list_products.top
                anchors.topMargin: 20
                anchors.bottom: area_list_products.bottom
                anchors.bottomMargin: 20
                anchors.right: area_list_products.right
                anchors.rightMargin: 20
                anchors.left: area_list_products.left
                anchors.leftMargin: container_infor.width + 22

                Rectangle {
                    id: content_list
                    anchors.fill: parent
                    anchors.top: container_list.top
                    anchors.topMargin: 10
                    anchors.bottom: container_list.bottom
//                    anchors.bottomMargin: 20
                    anchors.right: container_list.right
                    anchors.rightMargin: 20
                    anchors.left: container_list.left
                    anchors.leftMargin: 30
//                    color: "green"
                    color: "#F9FAFA"
                    Label {
                        id: lblProducts
                        text: "Products"
                        font.pixelSize: 22
                        font.bold: true
                    }
                    Rectangle {
                        id: rec_line
                        color: "black"
                        anchors.top: lblProducts.top
                        anchors.topMargin: container_list.anchors.topMargin * 2
                        anchors.left: content_list.left
                        anchors.leftMargin: -30
                        width: container_list.width
                        height: 2
                    }
                    Rectangle {
                        id: content_products
                        anchors.fill: parent
                        anchors.top: lblProducts.top
                        anchors.topMargin: rec_line.anchors.topMargin + 20
                        anchors.right: content_list.right
                        anchors.rightMargin: 30
                        color: "#F9FAFA"
//                        color: "blue"
                        ListProducts {
//                            width: container_list.width - 100
//                            height: container_list.height // 800
                            anchors.fill: parent
                        }
                    }
                }

            }




        }


    }
}
