#ifndef PROCESSDATA_H
#define PROCESSDATA_H

#include <QObject>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDateTime>
#include "ProductsList.h"





class ProcessData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)

    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)

    Q_PROPERTY(double limitation READ limitation WRITE setLimitation NOTIFY limitationChanged)

    Q_PROPERTY(double connectionLimitation READ connectionLimitation WRITE setConnectionLimitation NOTIFY connectionLimitationChanged)

    Q_PROPERTY(double secondaryConnectionLimitation READ secondaryConnectionLimitation WRITE setSecondaryConnectionLimitation NOTIFY secondaryConnectionLimitationChanged)

    Q_PROPERTY(QString permission READ permission WRITE setPermission NOTIFY permissionChanged)
    Q_PROPERTY(QString permissionPrecisionLanding READ permissionPrecisionLanding WRITE setPermissionPrecisionLanding NOTIFY permissionPrecisionLandingChanged)
    Q_PROPERTY(QString permissionDelivery READ permissionDelivery WRITE setPermissionDelivery NOTIFY permissionDeliveryChanged)
    Q_PROPERTY(QString permissionGeoTagging READ permissionGeoTagging WRITE setPermissionGeoTagging NOTIFY permissionGeoTaggingChanged)
    Q_PROPERTY(QString permissionNtrip READ permissionNtrip WRITE setPermissionNtrip NOTIFY permissionNtripChanged)

public:
    explicit ProcessData(QObject *parent = nullptr);

    QString userName() const;
    void setUserName(const QString &newUserName);

    QString email() const;
    void setEmail(const QString &newEmail);

    double limitation() const;
    void setLimitation(double newLimitation);

    double connectionLimitation() const;
    void setConnectionLimitation(double newConnectionLimitation);

    double secondaryConnectionLimitation() const;
    void setSecondaryConnectionLimitation(double newSecondaryConnectionLimitation);

    QString permission() const;
    void setPermission(const QString &newPermission);

    QString permissionPrecisionLanding() const;
    void setPermissionPrecisionLanding(const QString &newPermissionPrecisionLanding);

    QString permissionDelivery() const;
    void setPermissionDelivery(const QString &newPermissionDelivery);

    QString permissionGeoTagging() const;
    void setPermissionGeoTagging(const QString &newPermissionGeoTagging);

    QString permissionNtrip() const;
    void setPermissionNtrip(const QString &newPermissionNtrip);

signals:
    void userNameChanged();
    void emailChanged();
    void limitationChanged();
    void connectionLimitationChanged();
    void secondaryConnectionLimitationChanged();
    void permissionChanged();
    void permissionPrecisionLandingChanged();
    void permissionDeliveryChanged();
    void permissionGeoTaggingChanged();
    void permissionNtripChanged();

    void dataProducts(QVector< DataProducts > m_dataProductsVector);

public slots:
    void processInforUser(QString informationUser);
    void processAllProducts(QString allProducts);

private:
    QString _userName;
    QString _email;
    double _limitation;
    double _connectionLimitation;
    double _secondaryConnectionLimitation;
    QString _permission;
    QString _permissionPrecisionLanding;
    QString _permissionDelivery;
    QString _permissionGeoTagging;
    QString _permissionNtrip;

private:
    QVector< DataProducts > m_dataProductsVector;

};

#endif // PROCESSDATA_H
