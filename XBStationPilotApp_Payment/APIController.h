#ifndef APICONTROLLER_H
#define APICONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QEventLoop>
#include <QDateTime>
#include <QTimer>



class APIController : public QObject
{
    Q_OBJECT
public:
    explicit APIController(QObject *parent = nullptr);
    QString getData(QByteArray type, QByteArray token, QString urlParam);
//    QString putData(QByteArray type, QByteArray token, QString urlParam, QJsonDocument doc);
    QString postData(QByteArray type, QByteArray token, QString urlParam, QJsonDocument doc);


public slots:
    void getInforUser();
    void getAllProducts();
    QString postCreateOrders(QString productName);
signals:
    void inforUser(QString informationUser);
    void products(QString allProducts);
//    void paymentProduct(QString payment);
};

#endif // APICONTROLLER_H
