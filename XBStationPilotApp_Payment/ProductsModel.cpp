#include "ProductsModel.h"
#include "ProductsList.h"

ProductsModel::ProductsModel(QObject *parent)
    : QAbstractListModel(parent) , mList(nullptr)
{
}

int ProductsModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !mList)
        return 0;

    return mList->dataProducts().size();
}

QVariant ProductsModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !mList)
        return QVariant();

    const DataProducts item = mList->dataProducts().at(index.row());
    switch (role) {
    case NameRole:
        return QVariant(item.name);
    case TitleRole:
        return QVariant(item.title);
    case DescriptionRole:
        return QVariant(item.description);
    case PriceRole:
        return QVariant(item.price);
//    case IdRole:
//        return QVariant(item.id);
//    case NameRole:
//        return QVariant(item.name);
//    case PriceRole:
//        return QVariant(item.price);
//    case ValueRole:
//        return QVariant(item.value);
//    case CreatedAtRole:
//        return QVariant(item.createdAt);
//    case UpdatedAtRole:
//        return QVariant(item.updatedAt);
//    case DeletedAtRole:
//        return QVariant(item.deletedAt);
//    case PermissionIdRole:
//        return QVariant(item.permissionId);
//    case FeatureIdRole:
//        return QVariant(item.featureId);
    }

    return QVariant();
}

bool ProductsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!mList)
        return false;

    DataProducts item = mList->dataProducts().at(index.row());
    switch (role) {
    case NameRole:
        item.name = value.toString();
        break;
    case TitleRole:
        item.title = value.toString();
        break;
    case DescriptionRole:
        item.description = value.toString();
        break;
    case PriceRole:
        item.price = value.toDouble();
        break;
//    case IdRole:
//        item.id = value.toDouble();
//        break;
//    case NameRole:
//        item.name = value.toString();
//        break;
//    case PriceRole:
//        item.price = value.toDouble();
//        break;
//    case ValueRole:
//        item.value = value.toDouble();
//        break;
//    case CreatedAtRole:
//        item.createdAt = value.toString();
//        break;
//    case UpdatedAtRole:
//        item.updatedAt = value.toString();
//        break;
//    case DeletedAtRole:
//        item.deletedAt = value.toString();
//        break;
//    case PermissionIdRole:
//        item.permissionId = value.toDouble();
//        break;
//    case FeatureIdRole:
//        item.featureId = value.toDouble();
//        break;
    }

    if (mList->setProductAt(index.row(), item)) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ProductsModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

QHash<int, QByteArray> ProductsModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[NameRole] = "name";
    names[TitleRole] = "title";
    names[DescriptionRole] = "description";
    names[PriceRole] = "price";
//    names[IdRole] = "id";
//    names[NameRole] = "name";
//    names[PriceRole] = "price";
//    names[ValueRole] = "value";
//    names[CreatedAtRole] = "createdAt";
//    names[UpdatedAtRole] = "updatedAt";
//    names[DeletedAtRole] = "deletedAt";
//    names[PermissionIdRole] = "permissionId";
//    names[FeatureIdRole] = "featureId";
    return names;
}

ProductsList *ProductsModel::list() const
{
    return mList;
}

void ProductsModel::setList(ProductsList *newList)
{
    beginResetModel();

    if (mList)
        mList->disconnect(this);

    mList = newList;

//    if (mList) {
//        qDebug() << "--> ProductsModel - setList: ";
//        connect(mList, &ProductsList::preProductAppended, this, [=]() {
//            const int index = mList->dataProducts().size();
//            beginInsertRows(QModelIndex(), index, index);
//            qDebug() << "--> ProductsModel - setList: ";
//        });
//        connect(mList, &ProductsList::postProductAppended, this, [=]() {
//            endInsertRows();
//        });

//        connect(mList, &ProductsList::preProductRemoved, this, [=](int dataSize) {
//            beginRemoveRows(QModelIndex(), 0, dataSize);
//            //            qDebug() << "setList - dataSize: " << dataSize;
//        });
//        connect(mList, &ProductsList::postProductRemoved, this, [=]() {
//            endRemoveRows();
//        });
//    }

    endResetModel();
}
