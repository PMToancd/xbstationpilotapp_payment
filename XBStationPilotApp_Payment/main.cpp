#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "APIController.h"
#include "ProcessData.h"
#include "ProductsList.h"
#include "ProductsModel.h"





int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/XBStationPilotApp_Payment/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);

    qmlRegisterType<ProductsModel>("productsModel", 1, 0, "ProductsModel");
    qmlRegisterUncreatableType<ProductsList>("Products", 1, 0, "ProductsList",
                                            QStringLiteral("ProductsList should not be created in QML"));

    APIController apiController;
    ProcessData processData;
    ProductsList productsList;

    QObject::connect(&apiController, &APIController::inforUser, &processData, &ProcessData::processInforUser);
    QObject::connect(&apiController, &APIController::products, &processData, &ProcessData::processAllProducts);

    QObject::connect(&processData, &ProcessData::dataProducts, &productsList, &ProductsList::appendProducts);

    engine.rootContext()->setContextProperty("processData", &processData);
    engine.rootContext()->setContextProperty("productsList", &productsList);
    engine.rootContext()->setContextProperty("apiController", &apiController);

    apiController.getInforUser();
    apiController.getAllProducts();
//    apiController.postOrders();

    engine.load(url);

    return app.exec();
}
