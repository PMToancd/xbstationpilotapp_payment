#include "APIController.h"

APIController::APIController(QObject *parent)
    : QObject{parent}
{

}

QString APIController::getData(QByteArray type, QByteArray token, QString urlParam)
{
    QNetworkAccessManager networkAccessManager;
    QUrl url(urlParam);
    QNetworkRequest request(url);
    request.setRawHeader(type, token);
    QNetworkReply *reply = networkAccessManager.get(request);
    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    timer.start(30000);   // 30 secs. timeout
    loop.exec();

    QString response = reply->readAll();
    return response;
    reply->deleteLater();

}

QString APIController::postData(QByteArray type, QByteArray token, QString urlParam, QJsonDocument doc)
{
    QNetworkAccessManager networkAccessManager;
    QUrl url(urlParam);
    QNetworkRequest request(url);
    request.setRawHeader(type, token);
    request.setRawHeader("Content-Type", "application/json");
    QNetworkReply *reply = networkAccessManager.post(request, doc.toJson());
    QEventLoop loop;
    QTimer timer;
    timer.setSingleShot(true);
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    timer.start(30000);   // 30 secs. timeout
    loop.exec();

    QString response = reply->readAll();
    return response;
    reply->deleteLater();
}

void APIController::getInforUser()
{
    QByteArray type = QByteArray("Authorization");
    QByteArray token = QByteArray("");
    QString urlStr = QString("https://server-demo.platform.xbstation.com:35443/users/6");
    QString informationUser = getData(type, token, urlStr);
    emit inforUser(informationUser);
//    qDebug() << "--> APIController - getInforUser: " << informationUser;
}

void APIController::getAllProducts()
{
    QByteArray type = QByteArray("Authorization");
    QByteArray token = QByteArray("");
    QString urlStr = QString("https://server-demo.platform.xbstation.com:35443/products");
    QString allProducts = getData(type, token, urlStr);
    emit products(allProducts);
    //    qDebug() << "--> APIController - getAllProducts: " << allProducts;
}

QString APIController::postCreateOrders(QString productName)
{

    QByteArray type = QByteArray("");
    QByteArray token = QByteArray("");
    QString urlStr = QString("https://server-demo.platform.xbstation.com:35443/orders");
    QJsonObject obj;
    obj.insert("userId", 5);
    obj.insert("productName", productName);
    QJsonDocument doc(obj);
    QString payment = postData(type, token, urlStr, doc);

    //process data
    QJsonDocument paymentProductDoc = QJsonDocument::fromJson(payment.toUtf8());
    QJsonObject paymentProductObj = paymentProductDoc.object();
    QString link = paymentProductObj.value("link").toString();
    return link;


//    qDebug() << "--> APIController - postCreateOrders: " << payment;
}
