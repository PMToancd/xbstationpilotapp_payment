#ifndef PRODUCTSMODEL_H
#define PRODUCTSMODEL_H

#include <QAbstractListModel>

Q_MOC_INCLUDE("ProductsList.h")

class ProductsList;

class ProductsModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(ProductsList *list READ list WRITE setList)

public:
    explicit ProductsModel(QObject *parent = nullptr);

    enum {
        NameRole,
        TitleRole,
        DescriptionRole,
        PriceRole
//        IdRole,
//        NameRole,
//        PriceRole,
//        ValueRole,
//        CreatedAtRole,
//        UpdatedAtRole,
//        DeletedAtRole,
//        PermissionIdRole,
//        FeatureIdRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

    ProductsList *list() const;
    void setList(ProductsList *newList);
signals:

private:
    ProductsList *mList;


};

#endif // PRODUCTSMODEL_H
