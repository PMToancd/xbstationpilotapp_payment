#include "ProcessData.h"


ProcessData::ProcessData(QObject *parent) : QObject{parent},
    _userName("Unknown"), _email("Unknown"), _limitation(0),
    _connectionLimitation(0), _secondaryConnectionLimitation(0), _permissionPrecisionLanding("Unknown"),
    _permissionDelivery("Unknown"), _permissionGeoTagging("Unknown"), _permissionNtrip("Unknown")
{

}

void ProcessData::processInforUser(QString informationUser)
{
    //    qDebug() << "--> ProcessData - processInforUser: " << informationUser;
    QJsonDocument inforUserDoc = QJsonDocument::fromJson(informationUser.toUtf8());
    QJsonObject inforUserObj = inforUserDoc.object();
    QJsonValue inforUserJsonValue = inforUserObj.value("data");
    QJsonObject inforUserJsonObject = inforUserJsonValue.toObject();

    _userName = inforUserJsonObject.value("userName").toString();
    _email = inforUserJsonObject.value("email").toString();
    _limitation = inforUserJsonObject.value("limitation").toDouble();
    _connectionLimitation = inforUserJsonObject.value("connectionLimitation").toDouble();
    _secondaryConnectionLimitation = inforUserJsonObject.value("secondaryConnectionLimitation").toDouble();
    _permission = inforUserJsonObject.value("permission").toString();
    if (_permission != nullptr) { //_permission="" tương đương _permission=nullptr
        if (_permission.size() >= 1) _permissionPrecisionLanding = _permission.at(0);
        if (_permission.size() >= 2) _permissionDelivery = _permission.at(1);
        if (_permission.size() >= 3) _permissionGeoTagging = _permission.at(2);
        if (_permission.size() >= 5) _permissionNtrip = _permission.at(4);
    }



    qDebug() << "--> ProcessData - processInforUser: " << "userName - " << _userName << " - email - " << _email << " - limitation - " << _limitation
             << " - connectionLimitation - " << _connectionLimitation << " - secondaryConnectionLimitation - " << _secondaryConnectionLimitation
             << " - permission - " << _permission << " - permissionPrecisionLanding - " << _permissionPrecisionLanding;

    emit this->userNameChanged();
    emit this->emailChanged();
    emit this->limitationChanged();
    emit this->connectionLimitationChanged();
    emit this->secondaryConnectionLimitationChanged();
    emit this->permissionChanged();
    emit this->permissionPrecisionLandingChanged();
    emit this->permissionDeliveryChanged();
    emit this->permissionGeoTaggingChanged();
    emit this->permissionNtripChanged();
}

void ProcessData::processAllProducts(QString allProducts)
{

//    qDebug() << "--> ProcessData - processAllProducts: " << allProducts;
    QJsonDocument allProductsDoc = QJsonDocument::fromJson(allProducts.toUtf8());
    QJsonArray allProductsArr = allProductsDoc.array();

    for (int i = 0; i < allProductsArr.size(); ++i) {
        QJsonObject productObj = allProductsArr.at(i).toObject();

//        double id = productObj.value("id").toDouble();
        QString name = productObj.value("name").toString();
        QString title = productObj.value("title").toString();
        QString description = productObj.value("description").toString();
        double price = productObj.value("price").toDouble();
//        double value = productObj.value("value").toDouble();
//        QString createdAt = productObj.value("createdAt").toString();
//        QString updatedAt = productObj.value("updatedAt").toString();
//        QString deletedAt = productObj.value("deletedAt").toString();
//        double permissionId = productObj.value("permissionId").toDouble();
//        double featureId = productObj.value("featureId").toDouble();

        qDebug() << "--> ProcessData - processAllProducts: " << "name - " << name << " - title - " << title
                 << " - description - " << description << " - price - " << price;
        m_dataProductsVector.append({name, title, description, price});

    }
    emit this->dataProducts(m_dataProductsVector);
}

QString ProcessData::permissionNtrip() const
{
    return _permissionNtrip;
}

void ProcessData::setPermissionNtrip(const QString &newPermissionNtrip)
{
    if (_permissionNtrip == newPermissionNtrip)
        return;
    _permissionNtrip = newPermissionNtrip;
//    emit permissionNtripChanged();
}

QString ProcessData::permissionGeoTagging() const
{
    return _permissionGeoTagging;
}

void ProcessData::setPermissionGeoTagging(const QString &newPermissionGeoTagging)
{
    if (_permissionGeoTagging == newPermissionGeoTagging)
        return;
    _permissionGeoTagging = newPermissionGeoTagging;
//    emit permissionGeoTaggingChanged();
}

QString ProcessData::permissionDelivery() const
{
    return _permissionDelivery;
}

void ProcessData::setPermissionDelivery(const QString &newPermissionDelivery)
{
    if (_permissionDelivery == newPermissionDelivery)
        return;
    _permissionDelivery = newPermissionDelivery;
//    emit permissionDeliveryChanged();
}

QString ProcessData::permissionPrecisionLanding() const
{
    return _permissionPrecisionLanding;
}

void ProcessData::setPermissionPrecisionLanding(const QString &newPermissionPrecisionLanding)
{
    if (_permissionPrecisionLanding == newPermissionPrecisionLanding)
        return;
    _permissionPrecisionLanding = newPermissionPrecisionLanding;
//    emit permissionPrecisionLandingChanged();
}

QString ProcessData::permission() const
{
    return _permission;
}

void ProcessData::setPermission(const QString &newPermission)
{
    if (_permission == newPermission)
        return;
    _permission = newPermission;
    //    emit permissionChanged();
}

double ProcessData::secondaryConnectionLimitation() const
{
    return _secondaryConnectionLimitation;
}

void ProcessData::setSecondaryConnectionLimitation(double newSecondaryConnectionLimitation)
{
    if (qFuzzyCompare(_secondaryConnectionLimitation, newSecondaryConnectionLimitation))
        return;
    _secondaryConnectionLimitation = newSecondaryConnectionLimitation;
    //    emit secondaryConnectionLimitationChanged();
}

double ProcessData::connectionLimitation() const
{
    return _connectionLimitation;
}

void ProcessData::setConnectionLimitation(double newConnectionLimitation)
{
    if (qFuzzyCompare(_connectionLimitation, newConnectionLimitation))
        return;
    _connectionLimitation = newConnectionLimitation;
    //    emit connectionLimitationChanged();
}

double ProcessData::limitation() const
{
    return _limitation;
}

void ProcessData::setLimitation(double newLimitation)
{
    if (qFuzzyCompare(_limitation, newLimitation))
        return;
    _limitation = newLimitation;
    //    emit limitationChanged();
}

QString ProcessData::email() const
{
    return _email;
}

void ProcessData::setEmail(const QString &newEmail)
{
    if (_email == newEmail)
        return;
    _email = newEmail;
    //    emit emailChanged();
}

QString ProcessData::userName() const
{
    return _userName;
}

void ProcessData::setUserName(const QString &newUserName)
{
    if (_userName == newUserName)
        return;
    _userName = newUserName;
    //    emit userNameChanged();
}
