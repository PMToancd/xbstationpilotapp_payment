import QtQuick 2.8
import QtQuick.Window 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.1

import productsModel 1.0

Rectangle {
    id: root
    //    color: "#F9FAFA"
    //    Layout.fillWidth: true
    anchors.fill: parent

    Component {
        id: delegateModel
        Rectangle {
            id: itemProduct
            color: "#F9FAFA" //#F9FAFA
            //            radius: 10
            width: root.width
            height: root.height / 3
            Rectangle {
                id: contain_item_product
                anchors.fill: parent
                anchors.bottom: itemProduct.bottom
                anchors.bottomMargin: 20
                radius: 10
                border.color: "#F0F0F0"
                border.width: 2
                Rectangle {
                    id: content_item_product
                    anchors.fill: parent
                    anchors.top: contain_item_product.top
                    anchors.topMargin: 10
                    anchors.right: contain_item_product.right
                    anchors.rightMargin: 10
                    anchors.left: contain_item_product.left
                    anchors.leftMargin: 10
                    anchors.bottom: contain_item_product.bottom
                    anchors.bottomMargin: 10
                    //                    color: "red"
                    Image {
                        id: imgProduct
                        height: content_item_product.height - 20
                        fillMode: Image.PreserveAspectFit
                        anchors.verticalCenter: content_item_product.verticalCenter
                        source: "https://res.cloudinary.com/doymgydia/image/upload/v1703319450/xb/package.png"
                    }
                    Label {
                        id: lblTitle
                        anchors.left: imgProduct.right
                        anchors.leftMargin: 40
                        text: model.title
                        font.bold: true
                        font.pixelSize: 18
                    }
                    Text {
                        id: txtDescription
                        anchors.fill: parent
                        anchors.top: lblTitle.top
                        anchors.topMargin: 40
                        anchors.right: contain_price_pay.left
                        anchors.rightMargin: imgProduct.width + 20
                        anchors.left: imgProduct.right
                        anchors.leftMargin: imgProduct.width + 40
                        text: model.description
                        wrapMode: Text.WordWrap
                        font.pixelSize: 14
//                        color: "blue"
                    }
                    Rectangle {
                        id: contain_price_pay
                        anchors.fill: content_item_product
                        //                        anchors.left: content_item_product.left //anchors.fill: content_item_product đã set các cạnh của contain_price_pay dính với
                        // các cạnh của content_item_product nên không cần anchors.left: content_item_product.left, qml cũng hiểu
                        anchors.leftMargin: content_item_product.width * 4 / 5
                        //                        color: "lightgreen"
                        Rectangle {
                            width: 2
                            height: contain_item_product.height
                            anchors.top: contain_price_pay.top
                            anchors.topMargin: -10
                            color: "#F0F0F0"
                        }
                        Label {
                            id: lblPrice
                            anchors.top: contain_price_pay.top
                            anchors.topMargin: contain_price_pay.height * 0.2
                            anchors.horizontalCenter: contain_price_pay.horizontalCenter
                            text: "$" + model.price
                            font.bold: true
                            font.pixelSize: 24
                        }
                        Button {
                            id: btnPay
                            width: contain_price_pay.width * 0.6
                            height: contain_price_pay.height * 0.2
                            anchors.horizontalCenter: contain_price_pay.horizontalCenter
                            anchors.verticalCenter: contain_price_pay.verticalCenter

                            text: "Pay"
                            background: Rectangle {
                                color: "#1D707D"
                            }
                            //                            font.bold: true
                            font.pixelSize: 16
                            onClicked: {
                                Qt.openUrlExternally(apiController.postCreateOrders(model.name))
                            }
                        }

                    }
                }
            }
        }
    }

    ListView {
        id: listViewProduct

        clip: true
        anchors.fill: parent
        model: ProductsModel {
            list: productsList
        }
        delegate: delegateModel
    }
}
