#include "ProductsList.h"

ProductsList::ProductsList(QObject *parent)
    : QObject{parent}
{

}

QVector<DataProducts> ProductsList::dataProducts() const
{
    return m_dataProducts;
}

bool ProductsList::setProductAt(int index, const DataProducts &product)
{
    if (index < 0 || index >= m_dataProducts.size())
        return false;

    const DataProducts &oldProduct = m_dataProducts.at(index);
//    if (product.id == oldProduct.id && product.name == oldProduct.name && product.price == oldProduct.price
//        && product.value == oldProduct.value && product.createdAt == oldProduct.createdAt && product.updatedAt == oldProduct.updatedAt
//        && product.deletedAt == oldProduct.deletedAt && product.permissionId == oldProduct.permissionId && product.featureId == oldProduct.featureId)
    if (product.name == oldProduct.name && product.title == oldProduct.title
        && product.description == oldProduct.description && product.price == oldProduct.price)
        return false;

    m_dataProducts[index] = product;
    return true;
}

void ProductsList::appendProducts(QVector<DataProducts> m_dataProductsVector)
{
//    emit preFlightRemoved(m_dataFlights.size());
//    m_dataFlights.clear();
//    emit postFlightRemoved();
//    qDebug() << "--> ProductsList - appendProducts: " << m_dataProductsVector.size();
    for (int i = 0; i < m_dataProductsVector.size(); ++i) {
//        emit preProductAppended();

        qDebug() << "--> ProductsList - appendProducts: " << m_dataProductsVector.at(i).title;
        m_dataProducts.append(m_dataProductsVector.at(i));

//        emit postProductAppended();
    }

}
